package at.aknapp.dev.autcarapp.drivingControl

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import at.aknapp.dev.autcarapp.R
import at.aknapp.dev.autcarapp.databinding.DrivingControlFragmentBinding
import at.aknapp.dev.autcarapp.model.ConnectionService

class DrivingControlFragment : Fragment() {

    companion object {
        fun newInstance() = DrivingControlFragment()
    }

    lateinit var viewModel: DrivingControlViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewModel = ViewModelProvider(this).get(DrivingControlViewModel::class.java)
        val binding: DrivingControlFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.driving_control_fragment, container, false)

        binding.drivingControlViewModel = viewModel
        binding.lifecycleOwner = this

        val intent = Intent(activity, ConnectionService::class.java)
        activity?.bindService(intent, viewModel.serviceConnection, Context.BIND_AUTO_CREATE)

        Log.i("DRIV", "${viewModel.autoMode.value}")

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        if (viewModel.mBinder.value != null) {
            activity?.unbindService(viewModel.serviceConnection)
        }
    }
}