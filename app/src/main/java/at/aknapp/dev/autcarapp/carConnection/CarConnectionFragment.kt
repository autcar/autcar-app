package at.aknapp.dev.autcarapp.carConnection

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import at.aknapp.dev.autcarapp.R
import at.aknapp.dev.autcarapp.databinding.CarConnectionFragmentBinding
import at.aknapp.dev.autcarapp.model.ConnectionService


class CarConnectionFragment : Fragment() {

    companion object {
        fun newInstance() = CarConnectionFragment()
    }

    lateinit var viewModel: CarConnectionViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewModel = ViewModelProvider(this).get(CarConnectionViewModel::class.java)
        val binding: CarConnectionFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.car_connection_fragment, container, false)

        binding.carConnectViewModel = viewModel
        binding.lifecycleOwner = this

        val intent = Intent(activity, ConnectionService::class.java)
        activity?.bindService(intent, viewModel.serviceConnection, Context.BIND_AUTO_CREATE)

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        if (viewModel.mBinder.value != null) {
            activity?.unbindService(viewModel.serviceConnection)
        }
    }


}