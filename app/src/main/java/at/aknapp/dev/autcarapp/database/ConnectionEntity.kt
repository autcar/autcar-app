package at.aknapp.dev.autcarapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "connection")
data class ConnectionEntity (
    @PrimaryKey(autoGenerate = true)
    var connectionId: Long = 0,

    @ColumnInfo(name = "autCarName")
    var autCarName: String = "",

    @ColumnInfo(name = "ip")
    var ipAddress: String = "127.0.0.1",

    @ColumnInfo(name = "port")
    var port: Short = 11000,

    @ColumnInfo(name = "lastConnection")
    var lastConnectionTime: Long = System.currentTimeMillis()
)